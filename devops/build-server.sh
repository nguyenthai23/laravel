#!/bin/bash

apt-get -qy update
apt-get -qy install curl git zip unzip 
docker-php-ext-install pdo_mysql ctype bcmath zip

sudo curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

apt-get -qy install npm
